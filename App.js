/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import HomePage from "./app/ui/pages/HomePage";

const App = () => {
    return (
        <HomePage/>
    );
};

export default App;
