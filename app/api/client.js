let api_key = '2f9cd6f371f5b423cdc9bb23ab389fbf'; //hardcoded for test purposes
export const searchMovie = (text, page = 0) => {
  let url =
    'https://api.themoviedb.org/3/search/movie?api_key=' +
    api_key +
    '&query=' +
    text +
    (page === 0 ? '' : '&page=' + page);
  url = encodeURI(url);
  return fetch(url)
    .then(result => result.json())
    .then(result => {
      return result;
    });
};

export const getMoviesGenres = () => {
  let url = 'https://api.themoviedb.org/3/genre/movie/list?api_key=' + api_key;
  url = encodeURI(url);
  return fetch(url).then(result => result.json());
};
