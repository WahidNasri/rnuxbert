export const Colors = {
  primaryColor: '#827f93',
  primaryTextColor: '#494949',
  accentColor: '#FF4081',
  secondaryTextColor: '#919191',
  invertedTextColor: '#fff',
  primaryBackgroundColor: '#f4f4f4',
  tabsColor: '#DFF7F6',
  disabledColor: '#eee',
  ratingColor: '#FDD835',
  tags: '#cdc',
  delete: '#937e7f',
};
