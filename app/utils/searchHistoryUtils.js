import AsyncStorage from '@react-native-community/async-storage';
import moment from "moment";

const KEY = 'searchHistory'; //AsyncStorage key of search history

//get all search history, returns an array of search records
export const getSearchHistory = async () => {
    try {
        const value = await AsyncStorage.getItem(KEY);
        let history = [];
        if (value != null) {
            history = JSON.parse(value);
        }
        //sort by time, descending
        history =  history.sort(function(a, b) {
            return (a.time > b.time) ? -1 : ((a.time < b.time) ? 1 : 0);
        });
        return history;
    } catch (e) {
        //if anything goes wrong, return an empty array.
        return [];
    }
};

/*
 * Add A text to the search History
 * If the same text was used already, we delete all previous values to maintain a list without duplicates
 */
export const addSearchHistory = async (text) => {
    //we add the search record and the time it was used
    let record = {text, time: moment().toISOString()};
    try {
        const value = await AsyncStorage.getItem(KEY);
        let history = [];
        if (value != null) {
            history = JSON.parse(value); //because the output of the AsyncStorage is a String, we need to parse it first.
            history = history.filter(rec => rec.text !== text); //we remove duplicates
        }
        history = [...history, record];

        await AsyncStorage.setItem(KEY, JSON.stringify(history));
    } catch (e) {
        console.log(e);
    }
};
//remove a search history record by the text.
export const removeSearchRecord = async (text) => {
    try {
        const value = await AsyncStorage.getItem(KEY);
        var history = [];
        if (value != null) {
            history = JSON.parse(value);
            history = history.filter(rec => rec.text !== text); //we remove the text
        }
        await AsyncStorage.setItem(KEY, JSON.stringify(history))
    }catch (e) {
        console.log(e);
    }
};

//clear all search history records.
export const clearHistory = async () => {
    try{
        await AsyncStorage.removeItem(KEY);
    }catch (e) {
        console.log(e);
    }
};
