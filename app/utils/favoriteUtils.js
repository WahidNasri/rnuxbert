import AsyncStorage from '@react-native-community/async-storage';
const KEY = 'favoriteMovies'; // AsyncStorage key for favorite movies

//Check if a movie is favorite, returns a boolean
export const isFavorite = async (movie) => {
    try {
        const value = await AsyncStorage.getItem(KEY);
        if (value != null) {
            let favorites = JSON.parse(value);
            let filtered = favorites.filter(m => m.id === movie.id);
            return filtered.length > 0;
        }

    } catch (e) {
        return false;
    }
};

//Toggle the favorite status of a movie, add it if not already added, remove it otherwise.
export const toggleFavorite = async (movie) => {
    try {
        const value = await AsyncStorage.getItem(KEY);
        var favorites = [];
        if (value != null) {
            favorites = JSON.parse(value);
        }
        if (await isFavorite(movie)) {
            favorites = favorites.filter(m => m.id !== movie.id);
        } else {
            favorites = [...favorites, movie];
        }
        await AsyncStorage.setItem(KEY, JSON.stringify(favorites))
    } catch (e) {
        console.log(e);
    }
};

//get all favorite movies, returns array of movies.
export const getFavorites = async () => {
    try {
        const value = await AsyncStorage.getItem('favoriteMovies');
        var favorites = [];
        if (value != null) {
            favorites = JSON.parse(value);
        }
       return favorites;
    } catch (e) {
        return [];
    }
};

//clear the favorites list.
export const clearFavorites = async () => {
    try{
        await AsyncStorage.removeItem("favoriteMovies");
    }catch (e) {
        console.log(e);
    }
};
