import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Image, TouchableOpacity, ImageBackground} from 'react-native';
import PropTypes from 'prop-types';
import Icon from "react-native-vector-icons/Ionicons";
import moment from "moment";
import {isFavorite, toggleFavorite} from "../../utils/favoriteUtils";
import Share from 'react-native-share';
import ImagePlaceholder from 'react-native-image-with-placeholder'
import ViewMoreText from 'react-native-view-more-text';
import {Colors} from "../../Constants";

// Created By wnasri At 04/12/2019 10:04
export default class MovieItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isFavorite: false
        }
    }

    componentDidMount() {
        isFavorite(this.props.movie).then(result => {
            this.setState({isFavorite: result})
        })
    }

    render() {
        const imagePlaceholder = <ImagePlaceholder
            style={styles.image}
            duration={1000}
            activityIndicatorProps={{size: 'large', color: 'green'}}
            src={'https://image.tmdb.org/t/p/original' + this.props.movie.poster_path}
            placeholder={'https://image.tmdb.org/t/p/original' + this.props.movie.backdrop_path}
        />;
        return (
            <View style={styles.container}>
                <ImageBackground source={require('../../img/movie_placeholder.png')} style={styles.image}>
                    <Image style={styles.image}
                           source={{uri: 'https://image.tmdb.org/t/p/original' + this.props.movie.backdrop_path}}/>
                </ImageBackground>
                <View style={styles.detailsContainer}>
                    <Text style={styles.title}>{this.props.movie.title}</Text>
                    <View style={{flexDirection: 'row'}}>
                        <Icon name={"ios-star"} color={Colors.ratingColor} size={15}/>
                        <Text style={styles.averageVote}>{this.props.movie.vote_average}</Text>
                        <Text style={styles.totalVoters}> ({this.props.movie.vote_count} Voters)</Text>
                    </View>
                    <Text>{moment(this.props.movie.release_date, 'YYYY-MM-DD').format('DD MMMM YYYY')}</Text>
                    {this.getGenresView()}
                    <ViewMoreText
                        numberOfLines={3}
                        renderViewMore={this.renderViewMore}
                        renderViewLess={this.renderViewLess}
                    >
                        <Text style={{alignSelf: 'stretch'}}>{this.props.movie.overview}</Text>
                    </ViewMoreText>
                    <View style={{flex: 1}}/>
                    {this.getFavoriteAndShareView()}
                </View>
            </View>
        )

    }
    renderViewMore(onPress){
        return(
            <Text style={{fontSize: 12, color: Colors.primaryColor}} onPress={onPress}>View more</Text>
        )
    };

    renderViewLess(onPress){
        return(
            <Text style={{fontSize: 12, color: Colors.primaryColor}} onPress={onPress}>View less</Text>
        )
    };

    getGenresView() {
        if (this.props.movie.genres) {
            return (
                <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
                    {this.props.movie.genres.map(genre => {
                        return (<Text style={styles.genre}>{genre.name}</Text>)
                    })}
                </View>
            )
        }
        return null;
    }

    getFavoriteAndShareView() {
        return (
            <View style={{flexDirection: 'row', alignSelf: 'flex-end'}}>
                <TouchableOpacity onPress={this.toggleFavorite} style={{paddingStart: 10, paddingEnd: 10}}>
                    <Icon name={this.state.isFavorite ? "ios-heart" : "ios-heart-empty"} color={Colors.primaryColor} size={25}/>
                </TouchableOpacity>
                <TouchableOpacity onPress={this.share} style={{paddingStart: 10, paddingEnd: 10}}>
                    <Icon name={Platform.OS === 'ios' ? "ios-share" : "md-share"} color={Colors.primaryColor} size={25}/>
                </TouchableOpacity>
            </View>
        )
    }

    toggleFavorite = () => {
        toggleFavorite(this.props.movie);
        this.props.onFavoriteChanged(!this.state.isFavorite);
        this.setState({isFavorite: !this.state.isFavorite})
    };
    share = () => {
        const shareOptions = {
            title: 'Share The Movie',
            message: this.props.movie.title,
            url: 'https://image.tmdb.org/t/p/original' + this.props.movie.backdrop_path,
        };
        Share.open(shareOptions)
            .then((shareOptions) => {
                console.log(res)
            })
            .catch((err) => {
                err && console.log(err);
            });
    }
}
MovieItem.propTypes = {
    movie: PropTypes.any,
    onFavoriteChanged: PropTypes.func
};
MovieItem.defaultProps = {
    onFavoriteChanged: (isFav) => {}
};
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.primaryBackgroundColor,
        borderColor: Colors.primaryBackgroundColor,
        borderRadius: 10,
        borderWidth: 2,
        padding: 10,
        flexDirection: 'row',
        minHeight: 300,
        margin: 10,
        ...Platform.select({
            ios: {},
            android: {
                elevation: 2,
            }
        })
    },
    image: {
        width: 150,
        height: '100%',
        borderRadius: 10,
        marginEnd: 10,
    },
    detailsContainer: {
        flex: 1,
        height: '100%'
    },
    title: {
        color: Colors.primaryTextColor,
        fontSize: 22
    },
    genre: {
        height: 25,
        borderRadius: 12,
        padding: 5,
        justifyContent: 'center',
        backgroundColor: Colors.tags,
        margin: 2,
        fontSize: 10
    },
    averageVote: {
        color: Colors.primaryTextColor,
        fontSize: 12,
        marginStart: 5
    },
    totalVoters: {
        color: Colors.secondaryTextColor,
        fontSize: 12
    }
});
