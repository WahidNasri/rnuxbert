import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import moment from "moment";
import PropTypes from 'prop-types';
import {Colors} from "../../Constants";
import Icon from "react-native-vector-icons/Feather";
import {removeSearchRecord} from "../../utils/searchHistoryUtils";

// Created By wnasri At 04/12/2019 13:06
export default class SearchHistoryItem extends Component {
    render() {
        return (
            <TouchableOpacity onPress={() => {this.props.onPress(this.props.searchRecord.text)}}>
                <View style={styles.container}>
                    <Text style={styles.text}>{this.props.searchRecord.text}</Text>
                    <Text>{moment(this.props.searchRecord.time).fromNow()}</Text>
                    <TouchableOpacity style={styles.delete} onPress={this.deleteRecord}>
                        <Icon color={Colors.delete} name={'trash'} size={15}/>
                    </TouchableOpacity>
                </View>
            </TouchableOpacity>
        );
    }
    deleteRecord = () => {
        removeSearchRecord(this.props.searchRecord.text).then(() => {
            this.props.onDeleted(this.props.searchRecord.text);
        })
    }
}
SearchHistoryItem.propTypes = {
    onPress: PropTypes.func,
    searchRecord: PropTypes.any,
    onDeleted: PropTypes.func
};
SearchHistoryItem.defaultProps = {
    onPress: (text) => {
    }
};
const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        paddingStart: 10
    },
    text: {
        flex: 1,
        color:Colors.primaryTextColor
    },
    delete: {
        paddingTop: 10,
        paddingBottom: 10,
        paddingStart: 20,
        paddingEnd: 20,
    }
});
