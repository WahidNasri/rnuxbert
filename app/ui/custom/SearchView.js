import React, {Component} from 'react';
import {
  Platform,
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import PropTypes from 'prop-types';
import {Colors} from '../../Constants';

// Created By wnasri At 04/12/2019 10:24
export default class SearchView extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <TouchableOpacity
          style={{width: 30}}
          onPress={() => {
            this.props.onClear();
          }}>
          {this.getSearchIcon()}
        </TouchableOpacity>
        <TextInput
          style={styles.textInput}
          onChangeText={this.props.onChangeText}
          value={this.props.value}
          placeholder={'Search...'}
          onSubmitEditing={e => {
            this.props.onSearch(this.props.value);
          }}
        />
        {this.getActionView()}
      </View>
    );
  }

  getSearchIcon() {
    if (this.props.value.trim() === '') {
      return <Icon size={25} name="ios-search" color={Colors.primaryColor} />;
    } else {
      return <Icon size={25} name="md-close" color={Colors.primaryColor} />;
    }
  }

  getActionView() {
    if (this.props.value.trim() === '') {
      return (
        <TouchableOpacity disabled={true}>
          <Icon
            size={25}
            name="ios-arrow-round-forward"
            color={Colors.disabledColor}
          />
        </TouchableOpacity>
      );
    } else {
      return (
        <TouchableOpacity
          disabled={false}
          onPress={() => {
            this.props.onSearch(this.props.value);
          }}>
          <Icon
            size={25}
            name="ios-arrow-round-forward"
            color={Colors.primaryTextColor}
          />
        </TouchableOpacity>
      );
    }
  }
}

SearchView.propTypes = {
  onSearch: PropTypes.func,
  onClear: PropTypes.func,
  value: PropTypes.string,
  onChangeText: PropTypes.func,
};
const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    height: 50,
    flexDirection: 'row',
    paddingStart: 10,
    paddingEnd: 10,
    margin: 10,
    ...Platform.select({
      ios: {
        borderRadius: 25,
        borderColor: '#eee',
        borderWidth: 2,
      },
      android: {
        elevation: 5,
      },
    }),
  },
  textInput: {
    flex: 1,
    height: '100%',
    color: Colors.primaryColor,
  },
});
