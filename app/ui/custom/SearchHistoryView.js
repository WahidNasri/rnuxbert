import React, {Component} from 'react';
import {Platform, ScrollView, StyleSheet, Text, View} from 'react-native';
import SearchHistoryItem from '../items/SearchHistoryItem';
import PropTypes from 'prop-types';
import {Colors} from '../../Constants';
// Created By wnasri At 05/12/2019 13:49
export default class SearchHistoryView extends Component {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        {this.props.searchHistory.length === 0 ? (
          <View style={{alignItems: 'center'}}>
            <Text style={styles.text1}>Welcome to my App.</Text>
            <Text style={styles.text1}>Try To search any movie.</Text>
            <Text style={styles.text1}>This Area will show the history of your search.</Text>
          </View>
        ) : (
          <View style={styles.historyContainer}>
            <Text style={styles.listTitle}>Search History</Text>
            <View style={styles.separator} />
            <ScrollView style={{alignSelf: 'stretch'}}>
              {this.props.searchHistory.map(record => {
                return (
                  <SearchHistoryItem
                      key={record.text}
                      searchRecord={record}
                      onDeleted={text => {this.props.onItemDeleted(text);}}
                    onPress={text => {this.props.onRecordPress(text);}}
                  />
                );
              })}
            </ScrollView>
          </View>
        )}
      </View>
    );
  }
}
SearchHistoryView.propTypes = {
  searchHistory: PropTypes.array,
  onRecordPress: PropTypes.func,
  onItemDeleted: PropTypes.func,
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.primaryBackgroundColor,
    margin: 20,
    borderRadius: 10,
  },
  text1: {
    color: Colors.primaryTextColor,
  },
  historyContainer: {
    alignSelf: 'stretch',
    paddingTop: 20,
    paddingBottom: 20,
    alignItems: 'center',
    flex: 1,
  },
  separator: {alignSelf: 'stretch', height: 1, backgroundColor: '#ccc'},
  listTitle: {marginBottom: 10, color: Colors.primaryColor},
});
