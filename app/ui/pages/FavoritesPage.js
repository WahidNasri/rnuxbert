import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  RefreshControl,
  ScrollView,
} from 'react-native';
import MovieItem from '../items/MovieItem';
import {getFavorites} from '../../utils/favoriteUtils';
import Icon from 'react-native-vector-icons/Ionicons';
import {Colors} from '../../Constants';
// Created By wnasri At 04/12/2019 13:22
export default class FavoritesPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      movies: [],
      refreshing: false,
    };
  }

  componentDidMount() {
    this.loadFavorites();
  }

  render() {
    return (
      <View style={styles.container}>
        {this.getEmptyPlaceholder()}
        {this.getMoviesVies()}
      </View>
    );
  }

  getMoviesVies() {
    return (
      <ScrollView
        style={{alignSelf: 'stretch'}}
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this.loadFavorites}
          />
        }>
        {this.state.movies.map(movie => {
          return (
            <MovieItem
              movie={movie}
              key={movie.id}
              onFavoriteChanged={isFavorite => {
                if (!isFavorite) {
                  this.setState({
                    movies: this.state.movies.filter(m => m.id !== movie.id),
                  });
                }
              }}
            />
          );
        })}
      </ScrollView>
    );
  }

  getEmptyPlaceholder() {
    if (this.state.movies.length > 0) {
      return null;
    }
    return (
      <ScrollView
        contentContainerStyle={{flexGrow: 1}}
        style={{alignSelf: 'stretch', height: '100%'}}
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this.loadFavorites}
          />
        }>
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            height: '100%',
          }}>
          <Icon size={100} color={Colors.primaryColor} name={'md-film'} />
          <Text style={styles.text1}>You don't have favorite movies?</Text>
          <Text style={styles.text1}>
            Search Further and definitely you will find your flavor
          </Text>
          <Text style={styles.text2}>(Pull to refresh your list)</Text>
        </View>
      </ScrollView>
    );
  }

  loadFavorites = () => {
    getFavorites()
      .then(results => {
        this.setState({movies: results});
      })
      .finally(() => {
        this.setState({refreshing: false});
      });
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  text: {
    color: Colors.primaryTextColor,
  },
  text2: {
    color: Colors.secondaryTextColor,
  },
});
