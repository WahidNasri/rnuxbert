import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Modal,
  ActivityIndicator,
} from 'react-native';
import MovieItem from '../items/MovieItem';
import SearchView from '../custom/SearchView';
import {getMoviesGenres, searchMovie} from '../../api/client';
import {
  addSearchHistory,
  getSearchHistory,
} from '../../utils/searchHistoryUtils';
import SearchHistoryView from '../custom/SearchHistoryView';
// Created By wnasri At 04/12/2019 10:19
export default class SearchPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchText: '',
      movies: [],
      searchHistory: [],
      totalPages: 0,
      currentPage: 0,
      loading: false,
    };
  }

  doSearch(text, addToHistory = true) {
    if (addToHistory) {
      //save to search history
      addSearchHistory(text);
    }
    this.setState({searchText: text, loading: true});
    //do the search
    //get genres first, to map the genre ids inside the movie object.
    getMoviesGenres()
      .then(result => {
        this.setState({genres: result.genres});
        searchMovie(text, this.state.currentPage + 1)
          .then(result => {
            this.setState({
              totalPages: result.total_pages,
              currentPage: result.page,
            });
            //map genres_ids to genres, to show the genres 'names' instead of ids.
            let moviesWithGenres = result.results.map(m => {
              return {
                ...m,
                genres: m.genre_ids.map(gid => {
                  return this.state.genres.filter(g => g.id === gid)[0];
                }),
              };
            });
            //check if it is a new search or a load more search
            if (addToHistory) {
              this.setState({movies: moviesWithGenres});
            } else {
              this.setState({
                movies: [...this.state.movies, ...moviesWithGenres],
              });
            }
          })
          .finally(() => {
            //eventually, hide loader
            this.setState({loading: false});
          });
      })
      .catch(err => {
        //if anything goes wrong, hide loader
        this.setState({loading: false});
      });
  }

  componentDidMount() {
    this.clear();
  }

  render() {
    return (
      <View style={styles.container}>
        {this.getLoadingView()}

        {this.getSearchHistoryView()}
        {this.getMoviesList()}
        <SearchView
          style={styles.searchView}
          value={this.state.searchText}
          onChangeText={txt => {
            this.setState({searchText: txt});
          }}
          onSearch={text => {
            this.doSearch(text, true);
          }}
          onClear={() => {
            this.clear();
          }}
        />
      </View>
    );
  }

  clear() {
    getSearchHistory().then(results => {
      this.setState({
        searchText: '',
        searchHistory: results,
        movies: [],
        currentPage: 0,
        totalPages: 0,
      });
    });
  }

  getLoadingView() {
    return (
      <Modal visible={this.state.loading} transparent={true}>
        <View style={styles.modalBackground}>
          <View style={styles.activityIndicatorWrapper}>
            <ActivityIndicator animating={this.state.loading} />
          </View>
        </View>
      </Modal>
    );
  }

  getMoviesList() {
    if (this.state.movies.length === 0) {
      return null;
    }
    return (
      <ScrollView style={{alignSelf: 'stretch'}}>
        <View style={{height: 70}} />
        {this.state.movies.map(movie => {
          return <MovieItem movie={movie} key={movie.id} />;
        })}
        {this.getLoadMoreButton()}
      </ScrollView>
    );
  }

  getLoadMoreButton() {
    if (this.state.currentPage >= this.state.totalPages) {
      return null;
    }
    return (
      <TouchableOpacity
        style={styles.loadMore}
        onPress={() => {
          this.doSearch(this.state.searchText, false);
        }}>
        <Text>
          Load More ({this.state.currentPage + 1}/{this.state.totalPages})
        </Text>
      </TouchableOpacity>
    );
  }

  getSearchHistoryView() {
    if (this.state.movies.length > 0) {
      return null;
    }
    return (
      <SearchHistoryView
        style={{marginTop: 90}}
        searchHistory={this.state.searchHistory}
        onRecordPress={text => {
          this.doSearch(text);
        }}
        onItemDeleted={text => {
          this.setState({
            searchHistory: this.state.searchHistory.filter(
              record => record.text !== text,
            ),
          });
        }}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  searchView: {
    position: 'absolute',
    top: 10,
    right: 20,
    left: 20,
  },
  loadMore: {
    paddingEnd: 10,
    paddingStart: 10,
    paddingTop: 5,
    paddingBottom: 5,
    backgroundColor: 'rgba(213,213,213,0.69)',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    marginBottom: 20,
  },
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040',
  },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF',
    height: 100,
    width: 100,
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
});
