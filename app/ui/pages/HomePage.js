import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import Icon from "react-native-vector-icons/Feather";
import {createBottomTabNavigator} from 'react-navigation-tabs'
import {TabBar} from 'react-native-animated-nav-tab-bar'
import {createAppContainer} from 'react-navigation'
import {createStackNavigator} from 'react-navigation-stack'
import SearchPage from "./SearchPage";
import FavoritesPage from "./FavoritesPage";
import {Colors} from "../../Constants";
// Created By wnasri At 04/12/2019 13:31

const TabBarIcon = (props) => {
    return (
        <Icon
            name={props.name}
            size={props.size ? props.size : 24}
            color={props.focused ? props.tintColor : "#222222"}

        />
    )
};

//initiate Search Tab Stack. With this approach each tab will have its own history stack.
const SearchStack = createStackNavigator(
    {
        Search: SearchPage
    },
    {
        defaultNavigationOptions: {
            title: 'Search',
            headerLayoutPreset: 'center',
            headerStyle: {
                backgroundColor: Colors.primaryColor,
            },
            headerTintColor: Colors.invertedTextColor,
        }
    }
);

//initiate Favorites Tab Stack. With this approach each tab will have its own history stack.
const FavoritesStack = createStackNavigator(
    {
        Favorites: FavoritesPage,
    },
    {
        defaultNavigationOptions: {
            title: 'Favorites',
            headerLayoutPreset: 'center',
            headerStyle: {
                backgroundColor: Colors.primaryColor,
            },
            headerTintColor: Colors.invertedTextColor,
        },
    }
);

//Set Search Tab icon
SearchStack.navigationOptions = {
    tabBarIcon: ({focused, tintColor}) => <TabBarIcon focused={focused} tintColor={tintColor} name="search"/>,
};

//Set Favorites Tab icon
FavoritesStack.navigationOptions = {
    tabBarIcon: ({focused, tintColor}) => <TabBarIcon focused={focused} tintColor={tintColor} name="heart"/>,
};

export default createAppContainer(
    createBottomTabNavigator(
        {
            Search: SearchStack,
            Favorites: FavoritesStack,
        }, {
            tabBarOptions: {
                activeTintColor: Colors.primaryColor,
                inactiveTintColor: "#222222",
            },
            tabBarComponent: props => <TabBar
                activeColors={Colors.primaryColor}
                activeTabBackgrounds={Colors.tabsColor}
                {...props}
            />,
        }
    )
)
